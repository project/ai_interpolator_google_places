As for any AI Interpolator plugin, the documentation is online at:
https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/ai-interpolator

This module was supported by FreelyGive (https://freelygive.io/), your partner in Drupal AI.
