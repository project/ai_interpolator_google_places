<?php

namespace Drupal\ai_interpolator_google_places\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure GooglePlaces API access.
 */
class GooglePlacesConfigForm extends ConfigFormBase {


  /**
   * Config settings.
   */
  const CONFIG_NAME = 'ai_interpolator_address.google_places_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_interpolator_address_google_places_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler needed to check for keys module.
   */
  final public function __construct(
    ConfigFactoryInterface $configFactory,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct($configFactory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  final public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    // Check if key module is enabled.
    $keysEnabled = $this->moduleHandler->moduleExists('key');

    // Disabled if key module is not available and set file location as default.
    $form['setting_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Setting Type'),
      '#description' => $this->t('You can setup to give your API Key in a text field or using the keys module.'),
      '#options' => [
        'api_key' => $this->t('API Key'),
        'key_module' => $this->t('Key Module'),
      ],
      '#default_value' => $config->get('setting_type') ?? 'api_key',
      '#disabled' => $keysEnabled ? FALSE : TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'password',
      '#title' => $this->t('GooglePlaces API Key'),
      '#description' => $this->t('Can be found and generated <a href="https://beta.google_places.ai/account" target="_blank">here</a>.'),
      '#default_value' => $config->get('api_key'),
      '#states' => [
        'visible' => [
          ':input[name="setting_type"]' => ['value' => 'api_key'],
        ],
      ],
    ];

    if ($keysEnabled) {
      // Only try to generate key_select types if the key module exist.
      $form['api_key_key'] = [
        '#type' => 'key_select',
        '#title' => $this->t('GooglePlaces API Key'),
        '#description' => $this->t('Select the key to use for the GooglePlaces API.'),
        '#default_value' => $config->get('api_key_key'),
        '#states' => [
          'visible' => [
            ':input[name="setting_type"]' => ['value' => 'key_module'],
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('setting_type', $form_state->getValue('setting_type'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_key_key', $form_state->getValue('api_key_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
