<?php

namespace Drupal\ai_interpolator_google_places\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\RuleBase;
use Drupal\ai_interpolator\PluginBaseClasses\SimpleTextChat;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_google_places\GooglePlacesApi;
use Drupal\ai_interpolator_openai\OpenAiRequester;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for an an address.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_search_address_text",
 *   title = @Translation("Search Address(es) Finder"),
 *   field_rule = "address"
 * )
 */
class SearchToAddress extends SimpleTextChat implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Search Address(es) Finder';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The OpenAI requester.
   */
  public OpenAiRequester $openAiRequester;

  /**
   * The Google Places API.
   */
  public GooglePlacesApi $googlePlacesApi;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\ai_interpolator_address\GooglePlacesApi $googlePlacesApi
   *   The Google Places requester.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    GooglePlacesApi $googlePlacesApi,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->googlePlacesApi = $googlePlacesApi;
  }

  /**
   * {@inheritDoc}
   */
  final public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ai_interpolator_google_places.api'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Do a Google Maps search and get back X amount of addresses.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Hotels near Trafalgar Square, London, UK";
  }

  /**
   * {@inheritDoc}
   */
  public function generateResponse($prompt, $interpolatorConfig, ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $fieldMask = 'places.displayName,places.addressComponents';
    $response = $this->googlePlacesApi->placesSearchApi($prompt, $fieldMask);
    $addresses = [];
    if (!empty($response['places'][0])) {
      foreach ($response['places'] as $place) {
        // Displayname is a must.
        if (!empty($place['displayName']['text'])) {
          $components = [];
          foreach ($place['addressComponents'] as $component) {
            $components[$component['types'][0] . '_long'] = $component['longText'];
            $components[$component['types'][0] . '_short'] = $component['shortText'];
          }
          $street = $components['route_long'] ?? '';
          if ($street) {
            $street .= !empty($components['street_number_short']) ? ' ' . $components['street_number_short'] : '';
          }
          $address = [
            'country_code' => $components['country_short'] ?? '',
            'administrative_area' => $components['administrative_area_level_1_long'] ?? '',
            'locality' => $components['postal_town_long'] ?? '',
            'postal_code' => $components['postal_code_long'] ?? '',
            'address_line1' => $street,
            'organization' => $place['displayName']['text'] ?? '',
          ];
        }
        $addresses[] = $address;
      }
    }
    return $addresses;
  }
}
