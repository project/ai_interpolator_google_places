<?php

declare(strict_types=1);

namespace Drupal\ai_interpolator_google_places\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\BooleanCheckboxWidget;
use Drupal\Core\Form\FormStateInterface;


/**
 * Defines the 'google_places_multifield' field widget.
 *
 * @FieldWidget(
 *   id = "google_places_multifield_widget",
 *   label = @Translation("Google Places Multifield"),
 *   field_types = {"google_places_multifield"},
 * )
 */
final class GooglePlacesMultifieldWidget extends BooleanCheckboxWidget {

}
